<?php include("db.php") ?>
<?php include("includes/header.php") ?>
<div class="container p-4">
    <div class="row">

        <div class="col-md-4">

            <?php
            if (isset($_SESSION['message'])) { ?>
                <div class="alert alert-<?= $_SESSION['message_type'] ?> alert-dismissible fade show" role="alert">
                    <?= $_SESSION['message'] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <?php session_unset();
            } ?>


            <div class="card card-body bg-dark text-white">
                <form action="save.php" method="POST">
                    <div class="form-group ">
                        <input type="text" name="titulo" class="form-control bg-dark text-white" placeholder="Titulo Tarea" autofocus>
                    </div>
                    <div class="form-group ">
                        <textarea name="descripcion" rows="7" class="form-control bg-dark text-white" placeholder="Descripcion Tarea"></textarea>
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="guardar" value="Guardar">
                </form>
            </div>
        </div>

        <div class="col-md-8">
            <table class="table table-striped table-dark">
                <thead class="thead-dark">
                    <tr>
                        <th>Titulo</th>
                        <th>Descripcion</th>
                        <th>Creacion</th>
                        <th>Accion</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = "SELECT * FROM tareas";
                    $result = mysqli_query($conn, $query);
                    while ($row = mysqli_fetch_array($result)) { ?>
                        <tr>
                            <td><?php echo $row['title'] ?></td>
                            <td><?php echo $row['descripcion'] ?></td>
                            <td><?php echo $row['created_at'] ?></td>
                            <td>

                                <a href="edit.php?id=<?php echo $row[id] ?>" class="btn btn-secondary">
                                    <i class="far fa-edit"></i>
                                </a>

                                <a href="delete.php?id=<?php echo $row[id] ?>" class="btn btn-danger">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>


                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php include("includes/footer.php") ?>